public class Main {

    public static void main(String[] args) {
        System.out.println("Count of production " + args[0]);
    int  countOfProduction = Integer.valueOf(args[0]);
        Production mas[] = new Production[countOfProduction + 1];
        for (int i = 0; i < args.length; i++) {
            if (i != 0) {
                switch (args[i]) {
                    case "tea":
                        mas[i] = new Tea();
                        break;
                    case "coffee":
                        mas[i] = new Coffee();

                        break;
                    default:
                        System.out.println("Error, entry correct name of production");
                        break;
                }

                System.out.println("                  ");
                System.out.println("Production " + i);
                mas[i].update();
                mas[i].read();
            }
        }

    }
}
