import java.util.UUID;

public class Credentials { //Класс для хранения и обработки персональных данных
    private UUID idPeople;
    private String surname;
    private String name;
    private String patronymic;
    private String mail;

    Credentials(){
        this.idPeople=UUID.randomUUID();
    }
    Credentials(String surname, String name, String patronymic, String mail) {
        if(this.idPeople!= null)
        this.idPeople = UUID.randomUUID();

        this.surname = surname;
        this.name = name;
        this.mail = mail;
    }


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public UUID getId() {
        return idPeople;
    }
}
