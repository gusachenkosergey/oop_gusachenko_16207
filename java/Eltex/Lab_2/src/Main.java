import java.util.Scanner;

public class Main {
    private static Main main = new Main();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, entry count of people");
        int count = sc.nextInt();
        Orders orders=new Orders();
            main.fillingData(count,orders);



    }


    public void fillingData(int count, Orders orders) {

        for (int i = 0; i < count; i++) {
            Credentials credentials = new Credentials();
            ShoppingCart shoppingCart = new ShoppingCart();
            Scanner sc = new Scanner(System.in);
            System.out.println("Please entry people data ");
            System.out.println(i + " Entry name of person");
            credentials.setName(sc.next());
            System.out.println(i + " Entry mail of person");
            credentials.setMail(sc.next());
            System.out.println(i + " Entry surname of person");
            credentials.setSurname(sc.next());
            System.out.println(i + " Entry patronymic of person");
            credentials.setPatronymic(sc.next());
            System.out.println("Please entry count of product");
            int productCount = sc.nextInt();

            for (int j = 0; j < productCount; j++) {
                System.out.println("Please entry product which you want to add in Shopping Cart");
                String product = sc.next();
                product.toLowerCase();
                switch (product) {
                    case "coffee":
                        Coffee coffee = new Coffee();
                        coffee.update();
                        System.out.println("Show your choice");
                        coffee.read();
                        shoppingCart.add(coffee);
                        break;
                    case "tea":
                        Tea tea = new Tea();
                        tea.update();
                        System.out.println("Show your choice");
                        tea.read();
                        shoppingCart.add(tea);
                        break;
                    default:
                        System.out.println("Please entry correct name of product");
                        break;
                }
            }
            System.out.println("Please entry creation time ");
            int creationTime = sc.nextInt();
            Order order =new Order(creationTime,shoppingCart,credentials);
            System.out.println("Please, wait... " + creationTime + " sec Your order is important to us");
            System.out.println();
            System.out.println("________________________________________________");
            System.out.println();
            orders.createPurchase(order);
            System.out.println();
            order.getShoppingCart().showHashSet();// not useful for this lab

            if (i == count - 1) {
                int itearation=0;
                while(!orders.getCartOrder().isEmpty()) {
                    System.out.println(itearation+" ITERATOIN");
                    System.out.println();

                    System.out.println("Show shopping cart");
                    orders.showAllOrders();
                    orders.checkOrders();
                    i++;
                }

            }

        }

    }


}