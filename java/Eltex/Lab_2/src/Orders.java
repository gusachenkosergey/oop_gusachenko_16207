import java.util.LinkedList;

public class Orders<P extends Order> {

    private Order order;
    private LinkedList<Order> cartOrder = new LinkedList<Order>();


    Orders() {
    }

    public void createPurchase(Order l) {
        if (l.isReady()) {
            cartOrder.add(l);
        }

    }

    public void checkOrders() {
        for (Order o : cartOrder) {
            if (o.isReady()) {
                cartOrder.remove(o);
            }
        }
    }

    public void showAllOrders() {
        for (Order o : cartOrder) {
            System.out.println(o);
            o.getShoppingCart().showShoppingCart();
        }
    }

    public void addOrder(Order o) {
        cartOrder.add(o);

    }

    public boolean isEmpty() {
        return cartOrder.isEmpty();
    }

    public LinkedList<Order> getCartOrder() {
        return cartOrder;
    }


}
