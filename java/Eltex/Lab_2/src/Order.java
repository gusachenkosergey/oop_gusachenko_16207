import java.util.concurrent.TimeUnit;

public class Order {
    private int creationTime;
    private ShoppingCart shoppingCart;
    private Credentials credentials;

    /**
     * boolean field which mean
     * (zero- order is waiting to creation)
     * (one- ready )
     */
    private boolean isReady = false;

    Order(int creationTime, ShoppingCart shoppingCart, Credentials credentials) {
        this.creationTime = creationTime;

        try {
            TimeUnit.MILLISECONDS.sleep(creationTime);
            this.shoppingCart = shoppingCart;
            this.credentials = credentials;
            TimeUnit.SECONDS.sleep(creationTime);
            isReady = true;

        } catch (InterruptedException e) {
            e.printStackTrace();

        }

    }

    public int getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(int creationTime) {
        this.creationTime = creationTime;
    }


    public boolean isReady() {
        return isReady;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }
}
