package com;
import java.io.Serializable;
import java.util.UUID;

public class Production implements ICrudAction,Serializable {
    static private int count;
    private UUID id;
    private String cost;
    private String name;
    private String country;
    private String firma;

    public Production(String name,String country,String cost){
        this.id=UUID.randomUUID();
        this.name=name;
        this.country=country;
        this.cost=cost;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma  ) {
        if(firma==null){
            firma="GENERATION";
        }
        this.firma = firma;
    }


    Production(){
        id = UUID.randomUUID();
    }

    Production(UUID id){
        this.id = id;
    }

    @Override
    public void create(){
        count++;
    }

    @Override
    public void read(){
        System.out.println("Name : "+ this.name);
        System.out.println("ID : "+ this.getId());
        System.out.println("Country : "+this.country);
        System.out.println("firm : "+ this.firma);
    }

    @Override
    public  void update(){

    }

    @Override
    public void delete(){
        count--;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public UUID getId() {
        return id;
    }

}
