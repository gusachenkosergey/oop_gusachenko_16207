package com;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.UUID;

public class Orders<P extends Order> {

    private Order order;
    private final Object mutex = new Object();
    private LinkedList<Order> cartOrder = new LinkedList<Order>();


    public Orders() {
    }

    public void createPurchase(Order l) {//Нужна ли здесь проверка заказов на готовность ?
        cartOrder.add(l);
    }

    public boolean deleteOrder(int id) {
        for (Order o : cartOrder) {
            if (id == o.getIDorder()) {
                cartOrder.remove(o);
                return true;
            }
        }
        return false;
    }

    public void checkOrders() {
        for (Order o : cartOrder) {
            if (o.isReady()) {
                cartOrder.remove(o);
            }
        }
    }

    public void showAllOrders() {
        synchronized (mutex) {
            ListIterator<Order> list = cartOrder.listIterator();
            for (int i = 0; i < cartOrder.size(); i++) {
                System.out.println(cartOrder.get(i) + ": Index of Order : " + (i + 1));
                System.out.println(cartOrder.get(i).getOrdersID() + ": ID of Order");
                cartOrder.get(i).getShoppingCart().showShoppingCart();
            }
        }
    }

    public void addOrder(Order o) {
        cartOrder.add(o);

    }


    public Order findById(UUID id) {
        for (Order o : cartOrder) {
            if (id.equals(o.getOrdersID())) {
                return o;
            }
        }
        System.out.println("Order by this id not found");
        return null;
    }

    public boolean isEmpty() {
        return cartOrder.isEmpty();
    }

    public LinkedList<Order> getCartOrder() {
        return cartOrder;
    }

    public void setCartOrder(LinkedList<Order> cartOrder) {
        this.cartOrder = cartOrder;
    }

    public void deleteOrders() {
        for (Order o : cartOrder) {
            cartOrder.remove(o);
        }
    }

    public Order returnRandomOrder() {
        return cartOrder.getFirst();
    }

    public Order findOrder(int id) {
        for (Order o : cartOrder) {
            if (id == (o.getIDorder())) {
                return o;
            }

        }
        return null;
    }
}



