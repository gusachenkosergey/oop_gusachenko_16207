package com;
import java.io.IOException;
import java.util.Scanner;

public class FillingData {

    private Orders<Order> orders;
    private Integer countOfPeople;
    private ShoppingCart<Production> shoppingCart;
    private Credentials credentials;

    public FillingData(Orders<Order> orders, Integer countOfPeople, ShoppingCart<Production> shoppingCart, Credentials credentials) {
        this.countOfPeople = countOfPeople;
        this.credentials = credentials;
        this.shoppingCart = shoppingCart;
        this.orders = orders;
    }

    public void start() {
        for (int i = 0; i < countOfPeople; i++) {

            Scanner sc = new Scanner(System.in);
            System.out.println("Do you want automatic orders generations  ? [y/n]");
            String c = sc.next();
            if (c.equals("y")) {
                automaticGeneration();
            } else {
                manualGeneration(i);
            }
            if (i == countOfPeople - 1) {
                check();

                //
                System.out.println("This your orders ");
                System.out.println();
                System.out.println("______________________");
                orders.showAllOrders();

                managerWork();

            }

        }
    }

    private void managerWork() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Do you want work with JSON ? [ y / n ] ");
        String s = sc.next();
        if (s.equals("n")) {
            System.out.println("\nDo you want to serialize all objects ? [ y / n ]");
            String s2 = sc.next();
            if (s2.equals("y")) {
                serializeAll(orders);
            }
            System.out.println("\nDo you want to deserialize all objects [ y / n ] ?");
            String s1 = sc.next();
            if (s1.equals("y")) {
                deserializeAll(orders);
            }
            System.out.println("Do you want serialize object ? [ y / n ] ");
            String s6 = sc.next();
            if (s6.equals("y")) {
                orders.showAllOrders();
                System.out.println("________ ");
                System.out.println("Choose order which you want to serialize ? [ y / n ] ");
                int ind = sc.nextInt();
                serialize(orders, ind);
            }
            System.out.println("Do you want serialize object ? [ y / n ]  ");
            String s3 = sc.next();
            if (s3.equals("y")) {
                orders.showAllOrders();
                System.out.println("Choose order which you want to deserialize ? [ y / n ] ");
                int ind = sc.nextInt();
                deserializeJSON(orders, ind);
            }
        } else if (s.equals("y")) {
            System.out.println("\nDo you want to serialize all objects  JSON? [ y / n ]");
            String s2 = sc.next();
            if (s2.equals("y")) {
                serializeAllJSON(orders);
            }
            System.out.println("\nDo you want to deserialize all objects JSON[ y / n ] ?");
            String s1 = sc.next();
            if (s1.equals("y")) {
                deserializeAllJSON(orders);
            }
            System.out.println("Do you want serialize object JSON? [ y / n ] ");
            String s6 = sc.next();
            if (s6.equals("y")) {
                orders.showAllOrders();
                System.out.println("________ ");
                System.out.println("Choose order which you want to serialize JSON ? [ y / n ] ");
                int ind = sc.nextInt();
                serializeJSON(orders, ind);
            }
            System.out.println("Do you want serialize object JSON? [ y / n ]  ");
            String s3 = sc.next();
            if (s3.equals("y")) {
                orders.showAllOrders();
                System.out.println("Choose order which you want to deserialize JSON? [ y / n ] ");
                int ind = sc.nextInt();
                deserialize(orders, ind);
            }

        }


    }

    public void automaticGeneration() {
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < countOfPeople; i++) {
            System.out.println("Please entry count of dummy products");
            int dummyProducts = sc.nextInt();
            System.out.println("Please entry max value of creation time for dummy products  ");
            int dummyCreationTime = sc.nextInt();
            for (int l = 0; l < dummyProducts; l++) {
                CreateOrder createOrder = new CreateOrder(orders.getCartOrder(), dummyCreationTime);
                orders.showAllOrders();
            }

            System.out.println("Please entry creation time ");
            int creationTime = +sc.nextInt();
            Order order = new Order(creationTime, shoppingCart, credentials);
            System.out.println("Please, wait... " + creationTime + " sec Your order is important to us");
            System.out.println();
            System.out.println("________________________________________________");
            System.out.println();
            orders.createPurchase(order);

            System.out.println();
        }
        System.out.println("Do you want send this cart order to server ? [y/n]");
        String s=sc.next();
        if(s.equals("y")){
          Client client=  new Client(orders);
            try {
                client.clientWork();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void manualGeneration(Integer i) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Please entry people data ");
        System.out.println((i + 1) + " Entry name of person ");
        credentials.setName(sc.next());
        System.out.println((i + 1) + " Entry mail of person");
        credentials.setMail(sc.next());
        System.out.println((i + 1) + " Entry surname of person");
        credentials.setSurname(sc.next());
        System.out.println((i + 1) + " Entry patronymic of person");
        credentials.setPatronymic(sc.next());
        System.out.println("Please entry count of product");
        int productCount = sc.nextInt();

        for (int j = 0; j < productCount; j++) {
            System.out.println("Please entry product which you want to add in Shopping Cart");
            String product = sc.next();
            product.toLowerCase();
            switch (product) {
                case "coffee":
                    Coffee coffee = new Coffee();
                    coffee.update();
                    System.out.println("Show your choice");
                    coffee.read();
                    shoppingCart.add(coffee);
                    break;
                case "tea":
                    Tea tea = new Tea();
                    tea.update();
                    System.out.println("Show your choice");
                    tea.read();
                    shoppingCart.add(tea);
                    break;
                default:
                    System.out.println("Please entry correct name of product");
                    break;
            }
        }
    }

    public void check() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to start check orders ? [y/n]");
        String s = sc.next();
        if (s.equals("y")) {
            int iteration = 0;
            CheckReady checkReady = new CheckReady(orders.getCartOrder());
            CheckWaiting checkWaiting = new CheckWaiting(orders.getCartOrder());

            orders.showAllOrders();

            System.out.println(iteration + " ITERATION");
            System.out.println();
            checkWaiting.check();
            checkReady.check();
            System.out.println("Show shopping cart ");
            orders.showAllOrders();
            // orders.checkOrders();
        }
    }


    private void deserializeAllJSON(Orders<Order> orders) {
        ManagerOrderJSON managerOrderJSON = new ManagerOrderJSON(orders);
        try {
            managerOrderJSON.readAll();
        } catch (IOException e) {
            System.out.println("File is not found !");
            e.printStackTrace();
        }
    }

    private void serializeAllJSON(Orders<Order> orders) {
        ManagerOrderJSON managerOrderJSON = new ManagerOrderJSON(orders);
        try {
            managerOrderJSON.saveAll();
        } catch (IOException e) {
            System.out.printf("File is not found !");
            e.printStackTrace();
        }
    }

    private void deserializeJSON(Orders<Order> orders, int ind) {
        ind -= 1;
        ManagerOrderJSON managerOrderJSON = new ManagerOrderJSON(orders);
        try {
            managerOrderJSON.readById(orders.getCartOrder().get(ind).getOrdersID());
        } catch (IOException e) {
            System.out.println("File is not found !");
            e.printStackTrace();
        }
    }

    private void serializeJSON(Orders<Order> orders, int ind) {
        ind -= 1;
        ManagerOrderJSON manager = new ManagerOrderJSON(orders);
        manager.saveById(orders.getCartOrder().get(ind).getOrdersID());


    }


    private void deserializeAll(Orders<Order> orders) {
        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        managerOrderFile.readAll();
        System.out.println("Deserialize is successfully, it is your new Orders ");
        orders.showAllOrders();
    }


    private void deserialize(Orders<Order> orders, int index) {
        index -= index;
        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        managerOrderFile.readById(orders.getCartOrder().get(index).getOrdersID());

    }

    private void serializeAll(Orders<Order> orders) {
        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        managerOrderFile.saveAll();
        System.out.println("Serialize all objects is successfully !");

    }

    private void serialize(Orders<Order> orders, int index) {
        index -= index;
        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        System.out.println("TEST ID " + orders.getCartOrder().get(index).getOrdersID());
        managerOrderFile.saveById(orders.getCartOrder().get(index).getOrdersID());
        System.out.println("Serialize successfully OrderID : " + orders.getCartOrder().get(index).getOrdersID());
    }
}
