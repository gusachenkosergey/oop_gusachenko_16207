package com;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

public class ShoppingCart<P extends Production> implements Serializable {

    private ArrayList<Production> shoppingCart = new ArrayList<Production>();
    private HashSet<UUID> productOrigin = new HashSet<UUID>();


    public void add(Production o) {

        productOrigin.add(o.getId());// I will change in for huy s nim
        shoppingCart.add(o);
    }

    public void delete(Production o) {
        for (Production l : shoppingCart) {
            if (l.equals(o)) {
                shoppingCart.remove(l);
            }
        }
    }


    public void showShoppingCart() {
        for (Production l : shoppingCart) {
            if (l != null) {
                System.out.println(l);
                l.read();
                System.out.println("                         ");
            }
        }
    }
    public void showHashSet(){
        for (UUID id:productOrigin) {
            System.out.println(id+ " ID of Production by HashSet information ");
        }
    }

    public boolean getProductionByID(UUID id) {
        return productOrigin.contains(id);
    }

    public ArrayList<Production> getShoppingCart() {
        return shoppingCart;
    }

    public void deleteAll() {
        shoppingCart.clear();
    }
    public int getSize(){
        return shoppingCart.size();
    }
}

