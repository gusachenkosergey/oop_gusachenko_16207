package com;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    static int port = 3345; // Порт, такой же, как у сервера
    static String address = "192.168.0.125"; // Адрес сервера
    private Orders<Order> orders;

    public Client(Orders<Order> orders) {
        this.orders = orders;
    }

    public void clientWork() throws IOException {
        Scanner sc = new Scanner(System.in);
        try {

            InetAddress addr = InetAddress.getByName(address);
            System.out.println("Поключаемся к " + address + ":" + port + "...");
            Socket socket = new Socket(addr, port);

            DataOutputStream oos = new DataOutputStream(socket.getOutputStream());
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream outputStream = new ObjectOutputStream(os);
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);

            String answer = "y";
            while (answer.equals("y")) {
                orders.showAllOrders();
                System.out.println("\n Start to send order [y/n]?");
                String an = sc.next();
                outputStream.writeUTF(an);
                // String message = ois.readUTF();

                System.out.println("\n Which of orders do you want to send ? [Please entry num of Order]");
                int i = sc.nextInt();
                outputStream.writeObject(orders.getCartOrder().get(i - 1));
                System.out.println("\n The message is send !");
                String message = ois.readUTF();
                if (message.equals("Your order is ready ! It's creation time : ")) {
                    Order order = (Order) ois.readObject();
                    order.getInfo();
                    order.printCreationTime();

                    System.out.println("\n Send another order again ? [y/n]");
                    answer = sc.next();
                }
                outputStream.close();
                oos.flush();
                socket.close();

            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}