package com;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class CreateOrder {

    private Orders<Order> orders;
    private final Object mutex = new Object();
    private final LinkedList<Order> cartOrder;
    private int maxCreation;

    public CreateOrder(LinkedList<Order> cartOrder, int maxCreation) {
        this.cartOrder = cartOrder;
        this.maxCreation = maxCreation;
        try {
            int randomCreationTime = ((int) (Math.random() * maxCreation));
            TimeUnit.SECONDS.sleep(randomCreationTime);
        } catch (InterruptedException e) {
            System.out.println("16 string in Create Order");
            e.printStackTrace();
        }
    }


}
