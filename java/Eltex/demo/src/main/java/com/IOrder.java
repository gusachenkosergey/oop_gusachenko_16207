package com;
import java.io.IOException;
import java.util.UUID;

public interface IOrder {

    Order readById(UUID id) throws IOException;
    void saveById(UUID id);
    void readAll() throws IOException;
    void saveAll() throws IOException;

}
