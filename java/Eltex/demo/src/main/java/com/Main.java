package com;
import java.util.Scanner;

public class Main {
    private static Main main = new Main();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, entry count of people");
        int countOfPeople = sc.nextInt();
        Orders<Order> orders = new Orders<>();
        Credentials credentials = new Credentials();
        ShoppingCart<Production> shoppingCart = new ShoppingCart<>();
        FillingData fillingData = new FillingData(orders, countOfPeople, shoppingCart, credentials);
        fillingData.start();

    }
}


