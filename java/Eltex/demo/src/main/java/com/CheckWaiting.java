package com;
import java.util.LinkedList;

public class CheckWaiting extends ACheck {

    private final LinkedList<Order> cartOrder;
    private final Object mutex=new Object();

    public CheckWaiting(LinkedList<Order> cartOrder) {
        super(cartOrder);
        this.cartOrder = cartOrder;
    }

    @Override
    public void run() {
        synchronized (mutex) {
            for (int i = 0; i < cartOrder.size(); i++) {
                if (!cartOrder.get(i).isReady()) {
                    cartOrder.get(i).setReady(true);
                }

            }
        }

    }

    public void check() {
        Thread thread = new Thread(this);
        thread.start();
    }
}
