package com.gusachenko.demo;

import com.AutomaticGeneration;
import com.Order;
import com.Orders;
import com.ShoppingCart;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLOutput;
import java.util.LinkedList;
import java.util.UUID;

@RestController
public class GreetingController {
    private Orders<com.Order> orders = new Orders<>();
    private ShoppingCart shoppingCart=new ShoppingCart();
    private AutomaticGeneration automaticGeneration = new AutomaticGeneration(orders,shoppingCart);

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true) String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/")
    public LinkedList<Order> readAll(@RequestParam(name = "command", required = false) String command, @RequestParam(name = "order_id", required = false) String order_id,@RequestParam(name = "cart_id", required = false) String cart_id) {
        LinkedList<Order> orderLinkedList=orders.getCartOrder();
        LinkedList<Order> test=new LinkedList<>();
        if (command.equals("readAll")) {
            return orders.getCartOrder();
        } else if (command.equals("readById") && order_id != null) {
              if(test.isEmpty()) {
                  //UUID ida=UUID.fromString(order_id);
                  Integer id=Integer.parseInt(order_id);
                  System.out.println("ID is "+ order_id);
                  test.add(orders.findOrder(id));
                  return test;
              }else{
                  Integer id=Integer.parseInt(order_id);
                  orderLinkedList.removeFirst();
                 orderLinkedList.add(orders.findOrder(id));
                    return orderLinkedList;
              }
        } else if(command.equals("addToCard")){
            Integer id=Integer.parseInt(cart_id);
            Order order=new Order(id,this.shoppingCart);
            orderLinkedList.add(order);
            return orderLinkedList;
        }else if(command.equals("delById")){

            Integer id=Integer.parseInt(order_id);
            boolean order =orders.deleteOrder(id);
            if(!order){
                throw new OrderNotFoundException();
            }
        }

        return null;
    }


    /*@GetMapping("/readById")
    public Order readById(@RequestParam(name = "command", required = false) String name, Model model){

    }*/


}