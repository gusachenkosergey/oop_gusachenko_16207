package main.java;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class CreateOrder implements Runnable {

    private Orders<Order> orders;
    private final Object mutex=new Object();
    private final LinkedList<Order> cartOrder;
    private int maxCreation;

    public CreateOrder(LinkedList<Order> cartOrder, int maxCreation) {
        this.cartOrder = cartOrder;
        this.maxCreation = maxCreation;
        Thread thread = new Thread(this);
        try {
            int randomCreationTime = ((int) (Math.random() * maxCreation));
            TimeUnit.SECONDS.sleep(randomCreationTime);
        } catch (InterruptedException e) {
            System.out.println("16 string in Create Order");
            e.printStackTrace();
        }
        thread.start();
        System.out.println("НЕ ЗАШЕЛ");
    }

    @Override
    public void run() {
        System.out.println("ЗАШЕЛ");
        synchronized (mutex) {
            System.out.println("СИНХРОНАЙЗ");
            int randomCreationTime = ((int) (Math.random() * maxCreation));
            cartOrder.add(new Order(randomCreationTime));
        }

    }
}
