
import java.util.UUID;

public interface IOrder {

    void readById();
    void saveById(UUID id);
    void readAll();
    void saveAll();

}
