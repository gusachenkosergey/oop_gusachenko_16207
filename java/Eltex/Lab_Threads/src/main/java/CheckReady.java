package main.java;
import java.util.LinkedList;
import java.util.ListIterator;

public class CheckReady extends ACheck {

    private Orders<Order> orders;
    private  LinkedList<Order> cartOrder;
    private final Object mutex=new Object();


    public CheckReady(LinkedList<Order> cartOrder) {
        super(cartOrder);
    this.cartOrder=cartOrder;

    }

    @Override
    public void run() {
        synchronized (mutex) {
            ListIterator<Order> list = cartOrder.listIterator();
            for (int i = 0; i < cartOrder.size(); i++) {
                if (cartOrder.get(i).isReady()) {
                    cartOrder.remove(i);
                }

            }
        }

    }

    public void check() {
        Thread thread = new Thread(this);
        thread.start();
        }

}

