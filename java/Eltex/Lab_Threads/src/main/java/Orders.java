package main.java;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.UUID;

public class Orders<P extends Order> {

    private Order order;
    private final Object mutex = new Object();
    private LinkedList<Order> cartOrder = new LinkedList<Order>();


    Orders() {
    }

    public void createPurchase(Order l) {//Нужна ли здесь проверка заказов на готовность ?
        cartOrder.add(l);
    }


    public void checkOrders() {
        for (Order o : cartOrder) {
            if (o.isReady()) {
                cartOrder.remove(o);
            }
        }
    }

    public void showAllOrders() {
        synchronized (mutex) {
            ListIterator<Order> list = cartOrder.listIterator();
            for (int i = 0; i < cartOrder.size(); i++) {
                System.out.println(cartOrder.get(i));
                cartOrder.get(i).getShoppingCart().showShoppingCart();
            }
        }
    }

    public void addOrder(Order o) {
        cartOrder.add(o);

    }

    public boolean isEmpty() {
        return cartOrder.isEmpty();
    }

    public LinkedList<Order> getCartOrder() {
        return cartOrder;
    }

    public Order findById(UUID id) {
        for (Order o : cartOrder) {
            if (id == o.getId(o)) {
                return o;
            } else {
                System.out.println("Order by this id not found");
                return null;
            }

        }
        System.out.println("Order by this id not found");
        return null;
    }


}
