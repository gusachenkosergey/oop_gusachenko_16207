
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Order {
    private int creationTime;
    private ShoppingCart shoppingCart;
    private final Object mutex = new Object();
    private Credentials credentials;
    private UUID id;
    /**
     * boolean field which mean
     * (zero- order is waiting to creation)
     * (one- ready )
     */
    private boolean isReady = false;

    public Order(int creationTime) {
        this.creationTime = creationTime;
        try {
            TimeUnit.SECONDS.sleep(creationTime);
            isReady = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.shoppingCart = new ShoppingCart();
        add();

    }

    Order(int creationTime, ShoppingCart shoppingCart, Credentials credentials) {
        this.creationTime = creationTime;
         this.id = UUID.randomUUID();

        try {
            TimeUnit.MILLISECONDS.sleep(creationTime);
            this.shoppingCart = shoppingCart;
            this.credentials = credentials;
            TimeUnit.SECONDS.sleep(creationTime);
            isReady = true;

        } catch (InterruptedException e) {
            e.printStackTrace();

        }

    }

    public void add() {
        shoppingCart.add(new Production("DUMMY PRODUCT", "LA", "127"));

    }

    public int getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(int creationTime) {
        this.creationTime = creationTime;
    }


    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean r) {
        this.isReady = r;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }
    public UUID getId(Order order){
        return  order.id;
    }

}
