import java.io.*;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder {
    private Orders<Order> orders;
    private LinkedList<Order> newCartOrder = new LinkedList<Order>();

    public ManagerOrderFile(Orders<Order> orders) {
        super();
        this.orders = orders;
    }

    @Override
    public Order readById(UUID id) {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();

        try (FileInputStream fis = new FileInputStream(fileName)) {
            ObjectInputStream oin = new ObjectInputStream(fis);
            Order order = (Order) oin.readObject();
            while (order != null) {
                if (order.getOrdersID().equals(id)) {
                    System.out.println(" Order is found ");
                    order.getInfo();
                    return order;
                } else {

                    System.out.println("Order is not found");
                }
                order = (Order) oin.readObject();
            }
        } catch (IOException e) {
            System.out.println(" File is not found " + fileName);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void saveById(UUID id) {

        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();

        System.out.println("ID PRODUCT IN SAVE BY ID : " + id);

        try {
            orders.showAllOrders();
            FileOutputStream stream = new FileOutputStream(String.valueOf(Paths.get(fileName)));
            ObjectOutputStream outputStream = new ObjectOutputStream(stream);
            outputStream.writeObject(orders.findById(id));
            outputStream.flush();
            outputStream.close();

        } catch (java.io.IOException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void readAll() {// прочитать все заказы из файла
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();

        Order order = null;
        try (FileInputStream file = new FileInputStream(fileName)) {
            ObjectInputStream in = new ObjectInputStream(file);
            order = (Order) in.readObject();
            while (order.getShoppingCart().getSize() != 0) {
                newCartOrder.add(order);
                    order = (Order) in.readObject();
            }
            orders.setCartOrder(newCartOrder);
        } catch (IOException | ClassNotFoundException e1) {
            System.out.println("End of file");
        }
    }

    @Override
    public void saveAll() {//сохраняет все заказы в файл
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();

        try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
            ObjectOutputStream out = new ObjectOutputStream(outputStream);
            for (Order o : orders.getCartOrder()) {
                out.writeObject(o);
            }
            out.flush();
            out.close();

        } catch (IOException e) {
            System.out.println("File not found :" + fileName);
            e.printStackTrace();
        }
    }
}
