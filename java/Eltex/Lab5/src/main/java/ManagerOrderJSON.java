
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.UUID;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.parser.Token;

public class ManagerOrderJSON extends AManageOrder {

    private Orders<Order> orders;
    private LinkedList<Order> newCartOrder = new LinkedList<Order>();


    public ManagerOrderJSON(Orders<Order> orders) {
        this.orders = orders;
    }

    @Override
    public Order readById(UUID id) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();
        File file = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        Order order = null;
        try {
            order = mapper.readValue(file, Order.class);
        } catch (IOException e) {
            System.out.println("File is not found :" + fileName);
            e.printStackTrace();
        }
        while (order != null) {
            if (order.getOrdersID().equals(id)) {
                System.out.println("\n Order is found");
                order.getInfo();
                return order;
            } else {
                System.out.println("\n Order is not found ");

            }
            mapper.readValue(file, Order.class);
        }
        return null;
    }

    @Override
    public void saveById(UUID id) {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();
        File file = new File(fileName);
        System.out.println("SAVE BY ID JSON " + id);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file, orders.findById(id));
        } catch (IOException e) {
            System.out.println("File is not found :" + fileName);
            e.printStackTrace();
        }
    }

    @Override
    public void readAll() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();
        File file = new File(fileName);
      /*  ObjectMapper mapper = new ObjectMapper();
        Order order = (Order) mapper.readValue(file, Order.class);
        while (order != null) {
            newCartOrder.add(order);
            order = (Order)mapper.readValue(file, Order.class);

        }
        */
        try (FileInputStream fis = new FileInputStream(fileName)) {
            JsonFactory jf = new JsonFactory();
            JsonParser jp = jf.createParser(fis);
            jp.setCodec(new ObjectMapper());
            jp.nextToken();
            while (jp.hasCurrentToken()) {
                Order order = jp.readValueAs(Order.class);
                newCartOrder.add(order);
                jp.nextToken();
            }

        }
        orders.setCartOrder(newCartOrder);
        orders.showAllOrders();
    }

    @Override
    public void saveAll() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nPlease entry name of file ");
        String fileName = sc.next();
        File file = new File(fileName);
        System.out.println("\nSAVE ALL in JSON");
        ObjectMapper mapper = new ObjectMapper();
        for (Order order : orders.getCartOrder()) {
            mapper.writeValue(file, order);
        }
        System.out.println("\nSave was successfully");
    }
}
