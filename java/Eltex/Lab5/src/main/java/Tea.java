import java.util.Scanner;
import java.util.UUID;

public class Tea extends Production {
    private String typeOfpackagep;

    Tea() {
        super();
    }

    Tea(UUID id) {
        super(id);
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Cost is :" + getCost());
        System.out.println("name of firma : " + getFirma());
        System.out.println("manufacturer country\n" + getCountry());
        System.out.println("Type of package :" + typeOfpackagep);
        System.out.println("Id" + getId());
    }

    @Override
    public void update() {
        super.update();
        Scanner sc = new Scanner(System.in);
        System.out.println("Cost is :");
        super.setCost(sc.next());
        System.out.println("name of firma : ");
        super.setFirma(sc.next());
        System.out.println("manufacturer country\n");
        super.setCountry(sc.next());
        System.out.println("Type of package :");
        this.typeOfpackagep = sc.next();

    }

    @Override
    public void delete() {
        super.delete();

    }
}
