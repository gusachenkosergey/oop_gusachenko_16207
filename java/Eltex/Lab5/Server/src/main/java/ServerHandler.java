import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class ServerHandler implements Runnable {

    private Socket client;
    private ObjectInputStream ois;
    private ObjectOutputStream out;
    private InputStream is;
    private HashMap<Socket, ArrayList<Order>> clientsOrders = new HashMap<Socket, ArrayList<Order>>();
    private ArrayList<Order> orders = new ArrayList<>();
    private int numOfIteration;
    private  final Object mutex = new Object();

    public ServerHandler(ObjectInputStream ois, ObjectOutputStream out, InputStream is, Socket client) {
        this.client = client;
        this.ois = ois;
        this.out = out;
        this.is = is;
    }

    @Override
    public void run() {
        synchronized (mutex) {
            String answer = "n";
            clientsOrders.put(client, orders);
            while (true) {
                numOfIteration++;
                try {
                    answer = ois.readUTF();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (!answer.equals("y")) {
                    endOfWork(out, is, ois);

                    break;
                }
                System.out.println("Order is  :" + numOfIteration);
                Order order = null;
                try {
                    order = (Order) ois.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

                System.out.println("\n Получил объект");

                if (order != null) {
                    addOrder(order);
                    sendOrder(order, out);
                } else {
                    System.out.println("ERROR ORDER IS NULL !!!");
                }
            }
        }
    }

    public void endOfWork(ObjectOutputStream out, InputStream is, ObjectInputStream ois) {
        clientsOrders.forEach((Socket, Orders) -> {
            try {
                Socket.close();

                out.close();
                is.close();
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            clientsOrders.clear();
            numOfIteration = 0;
        });
    }

    public void addOrder(Order order) {
        order.getInfo();
        orders.add(order);
    }

    public void sendOrder(Order order, ObjectOutputStream out) {
        order.setReady(true);
        try {
            out.writeUTF("Your order is ready ! It's creation time : ");
            out.writeObject(order);
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }
}
