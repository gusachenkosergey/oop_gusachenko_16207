import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import static java.lang.System.out;

public class Server {

    private int namOfIteration;
    static int port = 3345;

    public void work() throws ClassNotFoundException {

        try {
            ServerSocket socket = new ServerSocket(port);

            out.println("Waiting to connect...");
            Socket client = socket.accept();
            out.println("\nClient is connected ");

            OutputStream outputStream = client.getOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(outputStream);
            InputStream is = client.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            ServerHandler clientHandler=new ServerHandler(ois, out, is,client);
            new Thread(clientHandler).start();
        } catch (IOException ex) {
            out.println("Ошибка ввода/вывода");
            ex.printStackTrace();
        }
    }



}