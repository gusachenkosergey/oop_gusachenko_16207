import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Order implements Serializable {
    private int creationTime;
    private ShoppingCart shoppingCart;
    private transient final Object mutex = new Object();
    private Credentials credentials;
    private UUID ordersID;
    /**
     * boolean field which mean
     * (zero- order is waiting to creation)
     * (one- ready )
     */
    private boolean isReady = false;
    public Order(){

    }
    public Order(int creationTime) {
        this.ordersID = UUID.randomUUID();
        this.creationTime = creationTime;
        try {
            TimeUnit.SECONDS.sleep(creationTime);
            isReady = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.shoppingCart = new ShoppingCart();
        add();

    }

    Order(int creationTime, ShoppingCart shoppingCart, Credentials credentials) {
        this.creationTime = creationTime;
        this.ordersID =UUID.randomUUID();
        try {
            TimeUnit.MILLISECONDS.sleep(creationTime);
            this.shoppingCart = shoppingCart;
            this.credentials = credentials;
            TimeUnit.SECONDS.sleep(creationTime);
            isReady = true;


        } catch (InterruptedException e) {
            e.printStackTrace();

        }

    }

    public void add() {
        shoppingCart.add(new Production("DUMMY PRODUCT", "LA", "127"));

    }
    public void printCreationTime(){
        System.out.println("\n Creation time is = "+creationTime);
    }


    public int getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(int creationTime) {
        this.creationTime = creationTime;
    }


    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean r) {
        this.isReady = r;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public UUID getOrdersID() {
        return this.ordersID;
    }
    public void getInfo(){
        System.out.println("ID : " + this.ordersID);
        System.out.println("     -_Shopping Cart-_    ");
        this.shoppingCart.showShoppingCart();
    }
}
