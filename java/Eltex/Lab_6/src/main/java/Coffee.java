import java.util.Scanner;
import java.util.UUID;

public class Coffee extends Production  {
    private String ZERNA[] = {"ARABICA", "ROBUSTA"};
    private int type;

    Coffee() {
        super();
    }

    Coffee(UUID id) {
        super(id);
    }

    @Override
    public void create() {
        super.create();
        type = (int) (Math.random() * ZERNA.length);
    }

    @Override
    public void read() {
        super.read();
        System.out.println("Зерно:" + ZERNA[type]);
        System.out.println("Cost is :" + getCost());
        System.out.println("name of firma : " + getFirma());
        System.out.println("manufacturer country\n" + getCountry());
    }

    @Override
    public void update() {
        super.update();
        Scanner sc = new Scanner(System.in);
        System.out.println("Cost is :");
        super.setCost(sc.next());
        System.out.println("name of firma : ");
        super.setFirma(sc.next());
        System.out.println("manufacturer country\n");
        super.setCountry(sc.next());

        System.out.println("Вид зерна [0-1]:");
        this.type = sc.nextInt();
    }

    @Override
    public void delete() {
        super.delete();
        type = 0;

    }

}
