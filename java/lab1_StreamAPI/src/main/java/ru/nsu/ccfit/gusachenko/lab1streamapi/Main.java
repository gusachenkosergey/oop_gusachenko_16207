package ru.nsu.ccfit.gusachenko.lab1streamapi;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length == 1) {
            AtomicLong count = new AtomicLong();
            Files.lines(Paths.get(args[0]))
                    .map(w -> w.split("[^\\p{L}0-9]+"))
                    .flatMap(Arrays::stream)
                    .peek(e -> count.incrementAndGet())
                    .collect(groupingBy(identity(), counting())).entrySet()
                    .stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                    .forEachOrdered(e -> System.out.println(String.format("%s, %d , %.2f%n ", e.getKey(), e.getValue(), (float) (e.getValue() * 100) / count.get())));
        } else {
            System.out.print("Wrong arguments,usage:|input|");
            return;
        }
    }
}










