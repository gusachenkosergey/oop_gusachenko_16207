package ru.nsu.ccfit.gusachenko.Lab_1;

public class Word implements Comparable<Word> {

    private String word;
    private int count;

    Word(String string, int count) {
        this.word = string;
        this.count = count;
    }

    public Word(String wrd) {
        this.word = wrd;
        this.count = 1;
    }

    public String getWord() {
        return this.word;
    }

    public int getCount() {
        return this.count;
    }

    public String IncIs(String string) {
        new Word(string, 1);
        return this.word;
    }

    public void IncrementWord() {
        this.count++;
    }

    @Override
    public int compareTo(Word o) {
        return o.count-count;
    }

    public Word incrementWord() {
        this.count++;
        return this;
    }
}