package ru.nsu.ccfit.gusachenko.Lab_1;

import java.io.*;

public class Main {
    private Reader reader;
    private Writer writer;

    public static void main(String[] args) {
        StatisticPrinter printer;
        try {
            if (args.length == 1) {
                printer = new StatisticPrinter(new File(args[0]));
            } else if (args.length == 2) {
                printer = new StatisticPrinter(new File(args[0]), new File(args[1]));
            } else {
                System.out.print("Wrong arguments,usage: |input| |out| ");
                return;
            }
            printer.print();
        } catch (IOException e) {
        }
    }
}
