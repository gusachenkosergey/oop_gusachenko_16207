package ru.nsu.ccfit.gusachenko.Lab_1;

import java.io.Reader;
import java.util.*;
import java.util.regex.Pattern;

public class CSV {
    private HashMap<String, Word> wordHashMap = new HashMap<>();
    private int count_word;


    public void WordReader(Reader reader) {
        Scanner scanner = new Scanner(reader);
        scanner.useDelimiter(Pattern.compile("[^\\p{L}0-9]+"));
        while (scanner.hasNext()) {
            String word = scanner.next();
            wordHashMap.merge(word, new Word(word, 1), (a, b) -> {
                a.IncrementWord();
                return a;
            });
            this.count_word++;
        }
    }

    public int AmountofWords() {
        return this.count_word;
    }

    public Collection<Word> getMap() {

        return wordHashMap.values();
    }
}