package ru.nsu.ccfit.gusachenko.Lab_1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StatisticPrinter {
    private Writer writer;
    private Reader reader;

    public void print() throws IOException {
        CSV file = new CSV();
        file.WordReader(reader);
        List<Word> MapList = new ArrayList<Word>(file.getMap());
        MapList.sort(Word::compareTo);
        for (Word word : MapList) {
            try {
                writer.write(String.format("%s, %d, %.2f%%\n", word.getWord(), word.getCount(), word.getCount() / (double) file.AmountofWords() * 100));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        reader.close();
        writer.close();
    }

    public StatisticPrinter(File in, File out) throws IOException {
        reader = new FileReader(in);
        writer = new FileWriter(out);
    }

    public StatisticPrinter(File file) throws FileNotFoundException {
        try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(System.out)) {
            reader = new FileReader(file);
            writer = new OutputStreamWriter(System.out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

