package ru.nsu.ccfit.gusachenko.Lab2.factory;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractStock<P extends Production> implements Stock<P> {
    private final List<P> production = new ArrayList<>();
    private final int size;

    public AbstractStock(int size) {
        this.size = size;
    }

    @Override
    public P getProduction() throws InterruptedException {
        synchronized (production) {

            while (production.isEmpty())
                production.wait();

            P p = production.remove(production.size() - 1);
            production.notify();
            return p;
        }
    }


    @Override
    public void addProduction(P production) throws InterruptedException {
        synchronized (this.production) {
            while(this.production.size() >= size)
                this.production.wait();

            this.production.add(production);
            this.production.notify();
        }
    }

    @Override
    public int getCount() {
        synchronized (production) {
            return production.size();
        }
    }

    @Override
    public int getSize() {
        return size;
    }
}
