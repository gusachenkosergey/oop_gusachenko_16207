package ru.nsu.ccfit.gusachenko.Lab2.factory.engine;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Production;

import java.util.concurrent.TimeUnit;

public class Engine extends Production {

    public Engine(int creationTime) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(creationTime);
    }
}
