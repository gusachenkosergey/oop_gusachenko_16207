package ru.nsu.ccfit.gusachenko.Lab2.factory.carcase;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Factory;

public class CarcaseFactory extends Factory<Carcase> {


    private final CarcaseProvider provider;

    public CarcaseFactory(int size) {
        super(1, "Carcases", new CarcaseStock(size));
        provider = new CarcaseProvider(stock);
        try {
            threadPool.addTask(new CarcaseProvider(stock));
        } catch (InterruptedException e) {
            e.printStackTrace();

        }

    }

    @Override
    public int getCount() {
        return provider.getCount();
    }
}
