package ru.nsu.ccfit.gusachenko.Lab2.factory.engine;

import ru.nsu.ccfit.gusachenko.Lab2.factory.AbstractStock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

import java.util.ArrayList;
import java.util.List;

public class EngineStock extends AbstractStock<Engine> {

    public EngineStock(int size) {
        super(size);
    }
}
