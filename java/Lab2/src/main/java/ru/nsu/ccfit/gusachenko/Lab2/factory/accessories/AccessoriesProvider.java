package ru.nsu.ccfit.gusachenko.Lab2.factory.accessories;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Provider;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

import java.util.concurrent.atomic.AtomicInteger;

public class AccessoriesProvider implements Provider<Accessories> {

    private final AtomicInteger count=new AtomicInteger();
    private final Stock<Accessories> stock;
    public AccessoriesProvider(Stock<Accessories> stock){
        this.stock=stock;
    }

    @Override
    public int getCount() {
        return count.get();
    }

    @Override
    public void run() {
        while(true){
            try {
                stock.addProduction(new Accessories(5));
                count.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
