package ru.nsu.ccfit.gusachenko.Lab2.factory.car;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Factory;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.accessories.Accessories;
import ru.nsu.ccfit.gusachenko.Lab2.factory.carcase.Carcase;
import ru.nsu.ccfit.gusachenko.Lab2.factory.engine.Engine;

import java.util.ArrayList;
import java.util.List;

public class CarFactory extends Factory<Car> {

    private final List<CarWorker> workers = new ArrayList<>();

    public CarFactory(int workersCount, int size, Stock<Accessories> accessoriesStock, Stock<Carcase> carcaseStock, Stock<Engine> engineStock) {
        super(workersCount, "Car", new CarStock(size));
        for (int i = 0; i < workersCount; i++) {
            workers.add(new CarWorker(stock, accessoriesStock, carcaseStock, engineStock));
        }
    }

    public void wakeUpWorker() throws InterruptedException {
        for (CarWorker worker : workers) {
            if(!worker.isInWork()){
                threadPool.addTask(worker);
            }
        }

    }

    @Override
    public int getCount() {
        int count = 0;
        for (CarWorker worker : workers) {
            count += worker.getCount();
        }
        return count;
    }
}
