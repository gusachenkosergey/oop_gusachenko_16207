package ru.nsu.ccfit.gusachenko.Lab2.factory.carcase;

import ru.nsu.ccfit.gusachenko.Lab2.factory.AbstractStock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

import java.util.ArrayList;
import java.util.List;

public class CarcaseStock extends AbstractStock<Carcase> {

    private final List<Carcase> carcases = new ArrayList<>();

    public CarcaseStock(int size) {
        super(size);
    }
}
