package ru.nsu.ccfit.gusachenko.Lab2.factory.accessories;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Production;

import java.util.concurrent.TimeUnit;

public class Accessories extends Production {
    public Accessories(int creationTime) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(creationTime);
    }
}
