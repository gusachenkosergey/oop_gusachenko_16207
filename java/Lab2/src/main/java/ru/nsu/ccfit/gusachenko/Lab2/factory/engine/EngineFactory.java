package ru.nsu.ccfit.gusachenko.Lab2.factory.engine;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Factory;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.accessories.AccessoriesProvider;

public class EngineFactory extends Factory<Engine> {

    private final EngineProvider provider;

    public EngineFactory(int size) {
        super(1, "Engine", new EngineStock(size));
        provider = new EngineProvider(stock);

        try {
            threadPool.addTask(new EngineProvider(stock));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getCount() {
        return provider.getCount();
    }
}
