package ru.nsu.ccfit.gusachenko.Lab2.factory.accessories;

import ru.nsu.ccfit.gusachenko.Lab2.factory.AbstractStock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

import java.util.ArrayList;
import java.util.List;

public class AccessoriesStock extends AbstractStock<Accessories> {

    public AccessoriesStock(int size) {
        super(size);
    }
}
