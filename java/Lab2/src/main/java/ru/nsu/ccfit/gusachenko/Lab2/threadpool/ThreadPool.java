package ru.nsu.ccfit.gusachenko.Lab2.threadpool;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool {
    private final ThreadGroup group;

    private final int threadsCount;
    private final List<ThreadTask> pool;

    public ThreadPool(int threadsCount, String poolName) {
        this.threadsCount = threadsCount;
        pool = new ArrayList<>(threadsCount);
        group = new ThreadGroup(poolName);
        for (int i = 0; i < threadsCount; i++) {
            pool.add(new ThreadTask(group, i));
        }
    }

    public void addTask(Runnable currentTask) throws InterruptedException {
        while (true) {
            synchronized (group) {
                for (ThreadTask task : pool) {
                    if (task.isFree()) {
                        task.setCurrentTask(currentTask);
                        return;
                    }
                }
                group.wait();
            }
        }

    }

    public void stopAll() {
        group.interrupt();
        for (ThreadTask task : pool) {
            try {
                task.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
