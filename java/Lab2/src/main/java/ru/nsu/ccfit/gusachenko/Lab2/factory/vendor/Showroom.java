package ru.nsu.ccfit.gusachenko.Lab2.factory.vendor;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Factory;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.accessories.AccessoriesProvider;
import ru.nsu.ccfit.gusachenko.Lab2.factory.car.Car;

import java.util.ArrayList;
import java.util.List;

public class Showroom extends Factory<Money> {

    private int countOfCars;
    private final List<Dealer> dealers= new ArrayList<>();

    public Showroom(Stock<Car> carStock, int dealersCount) {
        super(dealersCount, "Showroom", null);
        for (int i = 0; i < dealersCount; i++) {
            try {
                Dealer dealer= new Dealer(carStock);
                dealers.add(dealer);
                threadPool.addTask(dealer);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getCount() {
        int count = 0;
        for (Dealer dealer : dealers) {
            count += dealer.getCount();
        }
        return count;
    }
}
