package ru.nsu.ccfit.gusachenko.Lab2.factory.carcase;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Production;

import java.util.concurrent.TimeUnit;

public class Carcase extends Production {

    public Carcase(int creationTime) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(creationTime);
    }
}
