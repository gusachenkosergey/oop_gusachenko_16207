package ru.nsu.ccfit.gusachenko.Lab2.factory;

public interface Stock<P extends Production> {
    P getProduction() throws InterruptedException;

    void addProduction(P production) throws InterruptedException;

    int getCount();

    int getSize();
}
