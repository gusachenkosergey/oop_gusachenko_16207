package ru.nsu.ccfit.gusachenko.Lab2.factory.accessories;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Factory;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

import java.util.ArrayList;
import java.util.List;

public class AccessoriesFactory extends Factory<Accessories> {

    private final List<AccessoriesProvider> providers = new ArrayList<>();

    public AccessoriesFactory(int size, int countOfProvider) {
        super(countOfProvider, "Accessories", new AccessoriesStock(size));
        for (int i = 0; i < countOfProvider; i++) {
            try {
                AccessoriesProvider provider = new AccessoriesProvider(stock);
                providers.add(provider);
                threadPool.addTask(provider);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getCount() {
        int count = 0;
        for (AccessoriesProvider provider : providers) {
            count += provider.getCount();
        }
        return count;
    }
}
