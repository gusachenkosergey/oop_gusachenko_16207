package ru.nsu.ccfit.gusachenko.Lab2.factory.car;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

public class Controller extends Thread {

    private final CarFactory carFactory;
    private final Stock<Car> stock;

    public Controller(CarFactory carFactory) {
        this.carFactory = carFactory;
        stock = carFactory.getStock();
        start();
    }

    @Override
    public void run() {
        while (true) {
            if (stock.getCount() < 2 * carFactory.getWorkersCount()) {
                try {
                    carFactory.wakeUpWorker();
                } catch (InterruptedException e) {
                    return;
                }
                /**
                 * рочитать GridLayout прочитать про swing попробовать создать ))
                 * 
                 */
            }
        }
    }
}
