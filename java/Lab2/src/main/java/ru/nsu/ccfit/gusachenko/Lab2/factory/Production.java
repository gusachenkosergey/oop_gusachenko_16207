package ru.nsu.ccfit.gusachenko.Lab2.factory;

import java.util.UUID;

public abstract class Production {
    private final UUID id = UUID.randomUUID();

    public  UUID getID(){
     return id;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
