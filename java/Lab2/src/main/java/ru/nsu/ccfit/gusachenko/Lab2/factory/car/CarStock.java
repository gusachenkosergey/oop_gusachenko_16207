package ru.nsu.ccfit.gusachenko.Lab2.factory.car;

import ru.nsu.ccfit.gusachenko.Lab2.factory.AbstractStock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;

public class CarStock extends AbstractStock<Car> {
    public CarStock(int size) {
        super(size);
    }
}
