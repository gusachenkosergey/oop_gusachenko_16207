package ru.nsu.ccfit.gusachenko.Lab2.threadpool;

import java.util.concurrent.TimeUnit;

public class ThreadTask extends Thread {

    private volatile Runnable currentTask;
    private volatile boolean interrupted;
    private final Object mutex = new Object();//флаг на разделяемый ресурс (текущая задача)
    private final ThreadGroup group;

    public ThreadTask(ThreadGroup group, int id) {
        super(group, ThreadTask.class.getSimpleName() + "_" + id);
        this.group = group;
        start();e
    }

    @Override
    public void run() {
        try {

                    currentTask = null;
                    if (interrupted) {
                        return;
                    }
                    synchronized (group) {
                        group.notify();
                    }


                }

            }
        } catch (InterruptedException e) {

        }

    }

    @Override
    public void interrupt() { //нужно остановиться
        interrupted = true;
        super.interrupt();
    }

    public void setCurrentTask(Runnable currentTask) {
        synchronized (mutex) {//Блокировка потока в зависимости от выполнения другими( синхронайз)
            this.currentTask = currentTask;
            mutex.notify();
        }
    }

    public boolean isFree() {
        return currentTask == null;
    }


}
