package ru.nsu.ccfit.gusachenko.Lab2.factory;

public interface Provider<P extends Production> extends Runnable {

    int getCount();

}
