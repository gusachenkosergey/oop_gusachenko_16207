package ru.nsu.ccfit.gusachenko.Lab2.factory.carcase;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Provider;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.accessories.Accessories;

import java.util.concurrent.atomic.AtomicInteger;

public class CarcaseProvider implements Provider<Carcase> {

    private final AtomicInteger count = new AtomicInteger();
    private final Stock<Carcase> stock;

    public CarcaseProvider(Stock<Carcase> stock) {
        this.stock = stock;
    }

    @Override
    public int getCount() {
        return count.get();
    }

    @Override
    public void run() {
        while (true) {
            try {
                stock.addProduction(new Carcase(5));
                count.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

