package ru.nsu.ccfit.gusachenko.Lab2.factory.vendor;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Provider;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.car.Car;

import java.util.concurrent.atomic.AtomicInteger;

public class Dealer implements Provider<Money> {
    private final Stock<Car> stock;
    private final AtomicInteger count= new AtomicInteger();
    public Dealer(Stock<Car> carStock){
        this.stock=carStock;
    }

    @Override
    public int getCount() {
        return count.get();
    }

    @Override
    public void run() {
        while (true) {
            try {
                new Money(stock.getProduction(),5);
                 count.incrementAndGet();
                count.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
