package ru.nsu.ccfit.gusachenko.Lab2.factory.car;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Provider;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.accessories.Accessories;
import ru.nsu.ccfit.gusachenko.Lab2.factory.carcase.Carcase;
import ru.nsu.ccfit.gusachenko.Lab2.factory.engine.Engine;

import java.util.concurrent.atomic.AtomicInteger;

public class CarWorker implements Provider<Car> {

    private final AtomicInteger count = new AtomicInteger();
    private final Stock<Car> stock;
    private final Stock<Accessories> accessoriesStock;
    private final Stock<Carcase> carcaseStock;
    private final Stock<Engine> engineStock;
    private boolean inWork;

    public CarWorker(Stock<Car> stock, Stock<Accessories> accessoriesStock, Stock<Carcase> carcaseStock, Stock<Engine> engineStock) {

        this.stock = stock;
        this.accessoriesStock = accessoriesStock;
        this.carcaseStock = carcaseStock;
        this.engineStock = engineStock;
    }

    @Override
    public int getCount() {
        return count.get();
    }

    @Override
    public void run() {
        inWork = true;
        try {
            stock.addProduction(new Car(accessoriesStock.getProduction(), carcaseStock.getProduction(), engineStock.getProduction(), 5));

            count.incrementAndGet();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        inWork = false;
    }

    public boolean isInWork() {
        return inWork;
    }
}
