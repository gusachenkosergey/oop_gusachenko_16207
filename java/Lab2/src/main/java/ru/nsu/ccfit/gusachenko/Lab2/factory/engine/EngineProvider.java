package ru.nsu.ccfit.gusachenko.Lab2.factory.engine;

import ru.nsu.ccfit.gusachenko.Lab2.factory.Provider;
import ru.nsu.ccfit.gusachenko.Lab2.factory.Stock;
import ru.nsu.ccfit.gusachenko.Lab2.factory.accessories.Accessories;
import ru.nsu.ccfit.gusachenko.Lab2.factory.carcase.Carcase;

import java.util.concurrent.atomic.AtomicInteger;

public class EngineProvider implements Provider<Engine> {

    private final AtomicInteger count = new AtomicInteger();
    private final Stock<Engine> stock;


    public EngineProvider(Stock<Engine> stock) {
        this.stock = stock;
    }

    @Override
    public int getCount() {
        return count.get();
    }

    @Override
    public void run() {
        while (true) {
            try {
                stock.addProduction(new Engine(5));
                count.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
