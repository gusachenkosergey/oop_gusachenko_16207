package ru.nsu.ccfit.gusachenko.Lab2.factory;

import ru.nsu.ccfit.gusachenko.Lab2.threadpool.ThreadPool;

public abstract class Factory<P extends Production> {

    protected final ThreadPool threadPool;
    protected final Stock<P> stock;
    private final int workersCount;

    protected Factory(int workersCount, String factoryName, Stock<P> stock) {
        threadPool = new ThreadPool(workersCount, factoryName);
        this.stock = stock;
        this.workersCount = workersCount;
    }
    public void stop(){
        threadPool.stopAll();
    }

    public Stock<P> getStock() {
        return stock;
    }

    public int getWorkersCount(){
        return workersCount;
    }

    public abstract int getCount();
}
