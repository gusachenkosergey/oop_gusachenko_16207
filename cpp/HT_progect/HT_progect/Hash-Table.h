#include "Header.h"
class HashTable
{
	size_t HT_size = 0;//number of added element 
	const  size_t MAX_FULL = 75; //maximum degree of congestion
	HTVector *vec;
	size_t Vec_size = 0;//number of element which you can add
	size_t hash_fn(const Key str) const;
	void copyVec(HTVector *newVec, const HashTable &b);
	HTList::iterator find_list(const Key& k, HTVector *vec) const;
public:
	HashTable();
	friend bool operator==(const HashTable& a, const HashTable& b);

	friend bool operator!=(const HashTable & a, const HashTable & b);



	HashTable(const HashTable &other);

	HashTable(HashTable && other);
	HashTable & operator=(HashTable && other);
	void clear();
	void vec_clear(HTVector *newVec) const;
	bool erase(const Key& k);
	//add to container. Return value- succes of insertion.
	bool insert(const Key& k, const Value& v);


	// Checking the presence of a value for a given key
	bool contains(const Key& k) const;
	bool empty() const;
	//Return size HT (number of elements in it)
	size_t size() const;
	// Returns the value by key. An unsafe method.
	// If there is no key in the container, insert it into the container
	// The value created by the default constructor and return a reference to   
	Value& operator[](const Key& k);
	// Returns the value by key. Throws an exception on failure.
	Value& at(const Key& k);
	Value& HashTable::ht_at(const Key& k) const;
	Value& at(const Key& k) const;
	HashTable& operator=(const HashTable& b);
	void swap(HashTable& b);
	void resize();
	~HashTable();
};

HashTable createHashTable();
