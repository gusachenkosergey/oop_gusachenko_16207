#include "Hash-Table.h"

HashTable::HashTable()
{
	vec = new HTVector(7);
	Vec_size = 7;
	HT_size = 0;
}

void HashTable::copyVec(HTVector *newVec, const HashTable &b)//(2)->(1)
{

	for (size_t i = 0; i < b.vec->size(); i++)
	{
		if (!(*b.vec)[i]) {
			continue;
		}

		try
		{
			(*newVec)[i] = new HTList;
			for (HTList::iterator point_list = ((*b.vec)[i]->begin()); point_list != ((*b.vec)[i]->end()); point_list++) {
				(*newVec)[i]->push_back(make_pair(point_list->first, point_list->second));
			}
		}
		catch (bad_alloc &) {
			vec_clear(newVec);
			delete newVec;
			throw;
		}
	}
}

HashTable::HashTable(const HashTable & b)
{
	size_t Vec_size_old = b.Vec_size;
	size_t HT_size_old = 0;
	HTVector *vec_old = nullptr;

	vec_old = new HTVector(Vec_size_old);

	if (b.vec)
	{
		copyVec(vec_old, b);
	}

	Vec_size = Vec_size_old;
	HT_size = HT_size_old;
	vec = vec_old;

}

HashTable::HashTable(HashTable && other) :vec(other.vec), HT_size(other.HT_size)
{
	other.HT_size = 0;
	other.vec = nullptr;
}

HashTable & HashTable::operator=(HashTable && other)
{
	if (this != &other)
	{
		vec_clear(vec);
		delete vec;
		HT_size = other.HT_size;
		vec = other.vec;
		Vec_size = other.Vec_size;
		other.HT_size = 0;
		other.vec = nullptr;
	}
	return *this;
}

bool operator==(const HashTable & a, const HashTable & b)
{
	{
		if (&a == &b)
		{
			return true;
		}
		HTList::iterator point_list_A;
		HTList::iterator point_list_B;
		if (a.vec->size() != b.vec->size()) { return false; }

		for (size_t i = 0; i < a.vec->size(); i++)
		{
			if ((*a.vec)[i] != 0 && (*b.vec)[i] != 0)
			{
				point_list_A = (*a.vec)[i]->begin();
				point_list_B = (*b.vec)[i]->begin();


			}
			else
			{
				if ((*a.vec)[i] == 0 && (*b.vec)[i] == 0) { continue; }
				else { return false; }
			}
			if ((*a.vec)[i]->size() != (*b.vec)[i]->size()) { return false; }
			for (point_list_A = (*a.vec)[i]->begin(); point_list_A != (*a.vec)[i]->end(); point_list_A++, point_list_B++)
			{
				if (point_list_A->first != point_list_B->first || point_list_A->second.age != point_list_B->second.age || point_list_A->second.weight != point_list_B->second.weight) { return false; }
			}
		}
		return true;
	}

}

bool operator!=(const HashTable & a, const HashTable & b)
{
	return !(a == b);
}

size_t HashTable::hash_fn(const Key str) const
{
	hash<string> const h;
	size_t str_hash = h(str);
	return (h(str) % (*vec).size());
}

void HashTable::clear()
{
	if (vec) {
		vec_clear(vec);
		Vec_size = 7;
		HT_size = 0;
		vec->resize(7);
	}
}

void HashTable::vec_clear(HTVector *newVec) const
{
	for (int i = 0; i < vec->size(); i++)
	{
		if (!(*vec)[i]) { continue; }
		delete (*vec)[i];
		(*vec)[i] = nullptr;
	}
}

bool HashTable::erase(const Key& k)
{
	size_t h = hash_fn(k);
	if (!(*vec)[h])
	{
		return false;
	}
	HTList::iterator list = find_list(k, vec);
	if (list != (*vec)[h]->end())
	{
		if (list->first == k) {
			(*vec)[h]->erase(list);
			HT_size--;
			return true;
		}
		else {
			return false;
		}
	}
	return false;
}


HTList::iterator HashTable::find_list(const Key & k, HTVector * vec) const
{
	size_t h = hash_fn(k);
	for (HTList::iterator point_list = (*vec)[h]->begin(); point_list != (*vec)[h]->end(); point_list++)
	{
		if (point_list->first == k)
		{
			return point_list;
		}
	}
	return (*vec)[h]->end();
}

bool HashTable::insert(const Key & k, const Value & v)
{
	float proc = ((100 * (HT_size + 1)) / (vec->size()));
	if (proc > MAX_FULL) {
		resize();
	}
	HTPair pair = make_pair(k, v);
	size_t h = hash_fn(k);
	if (!(*vec)[h])
	{
		HTList* list = new HTList;

		(*list).push_back(pair);
		(*vec)[h] = list;
		HT_size++;

		return true;
	}

	HTList::iterator p_list = find_list(k, vec);
	if (p_list != (*vec)[h]->end())
	{
		p_list->second = v;
		return true;
	}
	else
	{
		(*vec)[h]->push_back(pair);
		HT_size++;
		return true;
	}

}

bool HashTable::contains(const Key & k) const
{

	int  h = hash_fn(k);
	if (!(*vec)[h])
	{
		return false;
	}
	HTList::iterator list = find_list(k, vec);
	if (list != (*vec)[h]->end())
	{
		return true;
	}
	return false;
}
bool HashTable::empty() const
{
	return !HT_size;
}

size_t HashTable::size() const
{
	return HT_size;
}

Value & HashTable::operator[](const Key & k)
{
	size_t h = hash_fn(k);
	if (!(*vec)[h])
	{
		Value tmp;
		HashTable::insert(k, tmp);
		return (*this)[k];
	}

	HTList::iterator list = find_list(k, vec);

	if (list->first == k) {
		return list->second;
	}

	(*vec)[h]->push_back(std::make_pair(k, Value{ 0, 0 }));
	HT_size++;
	return (*vec)[h]->back().second;
}

Value & HashTable::at(const Key & k)
{
	return (this)->ht_at(k);
}

Value& HashTable::ht_at(const Key & k) const
{
	size_t h = hash_fn(k);
	if (!(*vec)[h])
	{
		throw (out_of_range(k));
	}
	HTList::iterator list = find_list(k, vec);

	if (list->first == k) {
		return list->second;
	}

	throw (out_of_range(k));
}


Value & HashTable::at(const Key & k) const
{
	return(this)->ht_at(k);
}

HashTable & HashTable::operator=(const HashTable & b)
{

	if (this == &b)
	{
		return(*this);
	}

	HTVector *newVec = nullptr;

	try
	{
		newVec = new HTVector(b.vec->size());
		
	}
	catch (bad_alloc &)
	{
		vec_clear(newVec);
		delete newVec;
		throw;
	}
	copyVec(newVec, b);
	vec_clear(vec);
	delete vec;
	HT_size = b.HT_size;
	Vec_size = b.Vec_size;
	vec = newVec;

	return (*this);
}


void HashTable::swap(HashTable & b)
{
	std::swap(vec, b.vec);
	std::swap(HT_size, b.HT_size);
	std::swap(Vec_size, b.Vec_size);
}

void HashTable::resize()
{
	size_t oldVec = (*vec).size();
	HTVector *newVec = new HTVector(3 * oldVec);

	for (size_t i = 0; i < (*vec).size(); i++)
	{
		if (!(*vec)[i])
		{
			continue;
		}
		for (HTList::iterator point_list = (*vec)[i]->begin(); point_list != (*vec)[i]->end(); point_list++) {
			size_t hush = std::hash<Key>()(point_list->first) % (3 * (*vec).size());
			try
			{
				(*newVec)[hush] = new HTList;
				(*newVec)[hush]->push_back(make_pair(point_list->first, point_list->second));
			}
			catch (bad_alloc &) {

				vec_clear(newVec);
				delete newVec;
				throw;
			}
		}
	}
	vec_clear(vec);
	delete (vec);
	vec = newVec;
}

HashTable::~HashTable()
{
	clear();
	delete vec;
}