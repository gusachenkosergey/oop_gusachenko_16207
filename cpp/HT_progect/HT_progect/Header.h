#include<iostream>
#include<stdio.h>
#include <string>   
#include<iostream>
#include<stdio.h>
#include <string>
#include <vector>
#include <list>

struct Value {
	Value(unsigned age = 0, unsigned weight = 0) : age(age), weight(weight) {}
	unsigned age;
	unsigned weight;
	friend bool operator==(const Value& v1, const Value& v2) {
		return v1.age == v2.age && v1.weight == v2.weight;
	}
};

using namespace std;
using Key = std::string;
using HTPair = std::pair< Key, Value >;
using HTList = list< HTPair >;
using HTVector = vector<HTList*>;
