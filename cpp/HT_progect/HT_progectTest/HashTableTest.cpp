#include <gtest/gtest.h> 
#include "../HT_progect/Hash-Table.h"
//#include "../HT_progect/Hash-Table.h" 


class HashTableTest : public testing::Test
{

};

TEST_F(HashTableTest, First_Test)
{
	EXPECT_TRUE(5 > 0);
}
TEST_F(HashTableTest, Insert_Clear_Test)
{
	HashTable a;
	Value val;
	EXPECT_TRUE(a.empty());
	EXPECT_TRUE(a.insert("str", val));
	EXPECT_EQ(a.size(), 1);
	EXPECT_FALSE(a.empty());
	a.clear();
	EXPECT_TRUE(a.empty());
}


TEST_F(HashTableTest, Contains_and_at_Test)
{
	HashTable a;
	Value val, temp_val;
	a.insert("FIRST", val);
	EXPECT_FALSE(a.contains("SECOND"));
	EXPECT_TRUE(a.contains("FIRST"));
	temp_val = a.at("FIRST");
	EXPECT_EQ(val, temp_val);
}
TEST_F(HashTableTest, Erase_)
{
	HashTable a;
	Value val;
	a.insert("FIRST",val);
	EXPECT_FALSE(a.erase("SECOND"));
	EXPECT_TRUE(a.erase("FIRST"));
	EXPECT_TRUE(a.empty());
}
TEST_F(HashTableTest,Swap_ )
{
	HashTable a,b;
	Value val_a(19, 85);
	Value val_b(21, 16);
	a.insert("First", val_a);
	b.insert("Second", val_b);
	a.swap(b);
	EXPECT_EQ(a.size(), b.size());
	EXPECT_TRUE(a.contains("Second"));
	EXPECT_TRUE(b.contains("First"));
	EXPECT_FALSE(a.contains("First"));
	EXPECT_FALSE(b.contains("Second"));
	EXPECT_EQ(b.at("First"),val_a );
	EXPECT_EQ(a.at("Second"), val_b);
}
TEST_F(HashTableTest, Friend_operators_Test)
{
	HashTable a, b;
	Value val;
	a.insert("operator=", val);
	b.insert("operator=", val);
	EXPECT_TRUE(a==b);
	EXPECT_FALSE(a != b);
	Value val1(17, 17);
	b.insert("operator!=", val1);
	EXPECT_FALSE(a == b);
   EXPECT_TRUE(a != b);
}

TEST_F(HashTableTest, operator_Test)
{
	HashTable a, b;
	Value val;
	Value val1(17, 17);
	Value v;
	a.insert("operator=", val);
	b.insert("operator=", val1);
	a == b;
	a = b;
	//a = move(b); 
 	EXPECT_TRUE(a==b);
}

TEST_F(HashTableTest, Find_oper_HT)
{
	HashTable a;
	Value val, val1(100, 101);
	a.insert("operator", val1);
	val = a["operator"];
	EXPECT_EQ(val1, val);
}


TEST_F(HashTableTest,Resize_test)
{
	HashTable a;
	for (int i = 0; i <= 7; i++)
	{
		Value val(i, 17);
		a.insert("Str" + std::to_string(i),val);
	}
}

TEST_F(HashTableTest,Construct_move_test)
{
	HashTable a, b;
	Value val;
	Value val1(17, 17);
	Value v;
	a.insert("operator=", val);
	b.insert("operator=", val1);
	a == b;
	a = move(b);
	EXPECT_EQ(a.at("operator="), val1);
}

