#include <iostream>
#include "Header.h"
#include "Sort_String.h"


int main(int argc, char *argv[])
{
	if (argc > 3) 
	{
		cout << "Usage: sort - file.exe <input_file> <output_file>" << endl;
		return 1;
	}
	ifstream myfile;
	ofstream foutput;
	myfile.open(argv[1]);
	foutput.open(argv[2]);
	if (!myfile.is_open() || !foutput.is_open())
	{
		cout << "Error opening file";
	}
	string bufline;
	list <string> lst;
	if (myfile.is_open())
	{
		while (!myfile.eof())
		{
			getline(myfile, bufline);
			lst.push_back(bufline);//��� bufline 
		}
	
		using namespace sort_strings;
		sort_func(&lst);
		while (!lst.empty())
		{
			foutput << lst.back() << endl;
			lst.pop_back();
		}
	}
	myfile.close();
	system("pause");
	return 0;
}